#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#include <malloc.h>
#define MY_ENCODING_TYPE (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define KEYLENGTH 0x00800000
//--------------------------------------------------------------------
// These additional #define statements are required.
#define ENCRYPT_ALGORITHM CALG_RC4
#define ENCRYPT_BLOCK_SIZE 8
#define MAX_FILE_SIZE 4000000
#define SIGNATURE_SIZE 500

// 用来保存导出的公钥
INT eof = 0;
FILE *hSource;
FILE *hDestination;
FILE *hSignature;
HCRYPTPROV hCryptProv = 0;
HCRYPTKEY hKey = 0;
HCRYPTKEY hPubKey = 0;
HCRYPTHASH hHash = 0;
BYTE *pbSignature = NULL;
BYTE *pbKeyBlob;
PBYTE pbBuffer = NULL;
DWORD dwSigLen = 0;
DWORD dwBlobLen;
DWORD dwBufferLen = 0;
DWORD dwCount = 0;
DWORD dwBlockLen = 0;




//   Declare the functions. The function definition
//   follows main.
/*static BOOL VerifyFile (PCHAR szSource,PCHAR szDestination)
{

   char UserName;


    if(CryptAcquireContext(&hCryptProv,UserName,NULL,PROV_RSA_FULL,0))
    {
        printf("A crypto context with the %s key container \n", UserName);
        printf("has been acquired.\n");

    }
    else
    {
        if(CryptAcquireContext(&hCryptProv,UserName,NULL,PROV_RSA_FULL,CRYPT_NEWKEYSET))
        {
            printf("A new key container has been created.\n");
        }else
        {
            printf("Could not create a new key container.\n");
        }

    }


    if(CryptImportKey(hCryptProv, pbKeyBlob, dwBlobLen, 0, 0, &hPubKey))
        printf("导入公钥成功！\n");
    else
        printf("导入公钥失败！\n");

    //3.计算哈希

    if(CryptCreateHash(hCryptProv,CALG_MD5,0,0,&hHash))  //创建一个hash对象
        printf("创建哈希签名成功！\n");
    else
        printf("创建哈希签名失败！\n");

    pbBuffer = malloc(dwBufferLen);

    if(pbBuffer = (BYTE *)malloc(dwBufferLen))
    {
        printf("Memory has been allocated for the buffer. \n");
    }
    else
    {
        printf("Out of memory. \n");
    }
    dwCount = fread(pbBuffer, 1, dwBlockLen, hSource);


    if(CryptHashData(hHash,pbBuffer,dwCount,0)) //对一块数据进行哈希，把它加到指定的哈希对象中
        printf("将数据进行哈希成功！\n");
    else
        printf("将数据进行哈希失败！\n");

    //4.验证哈希签名
    if(CryptVerifySignature(hHash,pbSignature,dwSigLen,hPubKey,NULL,0))
        printf("验证签名成功！\n");
    else
        printf("验证签名失败！\n");


    if(pbBuffer)
        free(pbBuffer);
    if(hKey)
        CryptDestroyKey(hKey);

    if(hCryptProv)
        CryptReleaseContext(hCryptProv, 0);

    //return 0;

}*/
static BOOL SignFile   (PCHAR szSource,PCHAR szDestination)
{
    char UserName;

    if(hSource = fopen(szSource,"rb"))
    {
        printf("文件%s已经被打开. \n", szSource);
    }else
    {
        printf("打开失败!\n");
    }

    if(CryptAcquireContext(&hCryptProv,UserName,NULL,PROV_RSA_FULL,0))
    {
        printf("密钥容器已经创建\n");


    }
    else
    {
        if(CryptAcquireContext(&hCryptProv,UserName,NULL,PROV_RSA_FULL,CRYPT_NEWKEYSET))
        {
            printf("密钥容器已经创建\n");
        }else
        {
            printf("无法创建密钥容器\n");
        }

    }
       /* int res;
        res=CryptGenKey(hCryptProv,2,CRYPT_EXPORTABLE|0X04000000,&hKey); //创建一个ESA秘钥对
        if(!res)
        {
            printf("创建RSA秘钥对失败！\n");
            return FALSE;
        }*/


    CryptGetUserKey(hCryptProv,AT_SIGNATURE,&hKey);



    if (CryptExportKey(hKey,NULL,PUBLICKEYBLOB,0,NULL,&dwBlobLen))
        printf("获取公钥长度成功！\n");
    else
        printf("获取公钥失败！\n");
  /*  if(*pbKeyBlob=(BYTE*)malloc(dwBlobLen)) //开辟公钥长度空间
        printf("开辟公钥长度空间成功！\n");
    else
        printf("开辟公钥长度空间失败！\n");*/


    if (CryptExportKey(hKey,NULL,PUBLICKEYBLOB,0,pbKeyBlob,&dwBlobLen))
        printf("导出公钥成功！\n");
    else
        printf("导出公钥失败！\n");

 /*   if(CryptImportKey(hCryptProv, pbKeyBlob, dwBlobLen, 0, 0, &hPubKey))
        printf("导入公钥成功！\n");
    else
        printf("导入公钥失败！\n");*/



    if(CryptCreateHash(hCryptProv,CALG_MD5,0,0,&hHash))  //创建一个hash对象
        printf("创建哈希签名成功！\n");
    else
        printf("创建哈希签名失败！\n");

    pbBuffer = malloc(dwBufferLen);

    if(pbBuffer = (BYTE *)malloc(dwBufferLen))
    {
        printf("文件内容已输出到缓冲区. \n");
    }
    else
    {
        printf("输出出错！\n");
    }
    dwCount = fread(pbBuffer, 1, dwBlockLen, hSource);


    if(CryptHashData(hHash,pbBuffer,dwCount,0)) //对一块数据进行哈希，把它加到指定的哈希对象中
        printf("将数据进行哈希成功！\n");
    else
        printf("将数据进行哈希失败！\n");

    if(CryptSignHash(hHash,AT_SIGNATURE,NULL,0,pbSignature, &dwSigLen))
        printf("签名成功！\n");
    else
    {
        printf("签名失败！\n");
        return FALSE;
    }


    if(hHash)
        CryptDestroyHash(hHash);
    if(hKey)
        CryptDestroyKey(hKey);

    if(hCryptProv)
        CryptReleaseContext(hCryptProv, 0);


}

static BOOL DecryptFile (PCHAR szSource,PCHAR szDestination,PCHAR szPassword)
{
    char UserName;

    if(hSource = fopen(szSource,"rb"))
    {
        printf("文件%s已经被打开. \n", szSource);
    }else
    {
        printf("%s打开失败!\n", szSource);
    }
    if(hDestination = fopen(szDestination,"wb"))
    {
        printf("文件%s已经被打开. \n", szDestination);
    }else
    {
        printf("%s打开失败!\n", szDestination);
    }

    if(CryptAcquireContext(&hCryptProv,UserName,NULL,PROV_RSA_FULL,0))
    {
        printf("密钥容器已经创建\n");


    }
    else
    {
        if(CryptAcquireContext(&hCryptProv,UserName,NULL,PROV_RSA_FULL,CRYPT_NEWKEYSET))
        {
            printf("密钥容器已经创建\n");
        }else
        {
            printf("无法创建密钥容器\n");
        }

    }

    if(CryptCreateHash(hCryptProv,CALG_MD5, 0, 0, &hHash))
    {
        printf("创建哈希签名成功！\n");
    }else
    {
        printf("创建哈希签名失败！\n");
    }


    if(CryptHashData(hHash, (BYTE *)szPassword, strlen(szPassword), 0))
    {
        printf("将数据进行哈希成功！\n");
    }else
    {
        printf("将数据进行哈希失败！\n");
    }


    if(CryptDeriveKey(hCryptProv, ENCRYPT_ALGORITHM,hHash, KEYLENGTH, &hKey))
    {
        printf("解密密钥创建成功！\n");
    }else
    {
        printf("密钥创建失败！\n");
    }
    CryptDestroyHash(hHash);
    hHash = NULL;

    dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE;
    if(ENCRYPT_BLOCK_SIZE  > 1) {
            dwBufferLen = dwBlockLen + ENCRYPT_BLOCK_SIZE;
    } else
    {
        dwBufferLen = dwBlockLen;
    }
    pbBuffer = malloc(dwBufferLen);

    if(pbBuffer = (BYTE *)malloc(dwBufferLen))
    {
        printf("文件内容已输出到缓冲区. \n");
    }
    else
    {
        printf("输出失败！\n");
    }
    do {
    // 从源文件中读出dwBlockLen个字节
        dwCount = fread(pbBuffer, 1, dwBlockLen, hSource);
        if(ferror(hSource))
        {
            printf("读取失败\n");
        }

        if(!CryptDecrypt(hKey,0,feof(hSource),0,pbBuffer,&dwCount))

        {
            printf("读取失败\n");
        }

        fwrite(pbBuffer,1,dwCount,hDestination);
        if(ferror(hDestination))
        {
            printf("读取失败\n");
        }
    }

    while(!feof(hSource));
    {
        printf("OK\n");

    }
    if(hSource)
        fclose(hSource);
    if(hDestination)
        fclose(hDestination);
    if(pbBuffer)
        free(pbBuffer);
    if(hKey)
        CryptDestroyKey(hKey);
    if(hHash)
        CryptDestroyHash(hHash);
    if(hCryptProv)
        CryptReleaseContext(hCryptProv, 0);
   // return 0;

}


static BOOL EncryptFilessh(PCHAR szSource, PCHAR szDestination, PCHAR szPassword)
{
    char UserName;

    if(hSource = fopen(szSource,"rb"))
    {
        printf("文件%s已经被打开. \n", szSource);
    }else
    {
        printf("%s打开失败!\n",szSource);
    }
    if(hDestination = fopen(szDestination,"wb"))
    {
        printf("文件%s已经被打开. \n", szDestination);
    }else
    {
        printf("%s打开失败!\n", szDestination);
    }

    if(CryptAcquireContext(&hCryptProv,UserName,NULL,PROV_RSA_FULL,0))
    {
        printf("密钥容器已经创建\n");

    }
    else
    {
        if(CryptAcquireContext(&hCryptProv,UserName,NULL,PROV_RSA_FULL,CRYPT_NEWKEYSET))
        {
            printf("密钥容器已经创建\n");
        }else
        {
            printf("无法创建密钥容器\n");
        }

    }

    if(CryptCreateHash(hCryptProv,CALG_MD5, 0, 0, &hHash))
    {
        printf("创建哈希签名成功！\n");
    }else
    {
        printf("创建哈希签名失败！\n");
    }


    if(CryptHashData(hHash, (BYTE *)szPassword, strlen(szPassword), 0))
    {
        printf("将数据进行哈希成功！\n");
    }else
    {
        printf("将数据进行哈希失败！\n");
    }


    if(CryptDeriveKey(hCryptProv, ENCRYPT_ALGORITHM,hHash, KEYLENGTH, &hKey))
    {
        printf("加密密钥创建成功！\n");
    }else
    {
        printf("密钥创建失败！\n");
    }
    CryptDestroyHash(hHash);
    hHash = NULL;

    dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE;
    if(ENCRYPT_BLOCK_SIZE  > 1) {
            dwBufferLen = dwBlockLen + ENCRYPT_BLOCK_SIZE;
    } else
    {
        dwBufferLen = dwBlockLen;
    }
    pbBuffer = malloc(dwBufferLen);

    if(pbBuffer = (BYTE *)malloc(dwBufferLen))
    {
        printf("文件内容已输出到缓冲区. \n");
    }
    else
    {
        printf("输出失败！\n");
    }

    do {
    // 从源文件中读出dwBlockLen个字节
        dwCount = fread(pbBuffer, 1, dwBlockLen, hSource);
        if(ferror(hSource))
        {
            printf("读取失败\n");
        }

        if(!CryptEncrypt(hKey,0,feof(hSource),0,pbBuffer,&dwCount,dwBufferLen))
        {
            printf("读取失败\n");
        }

        fwrite(pbBuffer,1,dwCount,hDestination);
        if(ferror(hDestination))
        {
            printf("读取失败\n");
        }
    }

    while(!feof(hSource));
    {
        printf("OK\n");

    }
    if(hSource)
        fclose(hSource);
    if(hDestination)
        fclose(hDestination);
    if(pbBuffer)
        free(pbBuffer);
    if(hKey)
        CryptDestroyKey(hKey);
    if(hHash)
        CryptDestroyHash(hHash);
    if(hCryptProv)
        CryptReleaseContext(hCryptProv, 0);

   // return 0;
}

int main(void)
{
    CHAR szSource[100];
    CHAR szDestination[100];
    CHAR szPassword[100];


    while(1){
        printf("----------\n");
        printf("1.加密\n");
        printf("2.解密\n");
        printf("3.签名\n");
        printf("4.退出\n");
        printf("----------\n");
        int obt;
        printf("请输入数字:");
        scanf("%d",&obt);
        switch(obt){

            case 1:

             // Call EncryptFile to do the actual encryption.   加密文件
             printf("------------------------------------------------------------\n");
             printf("加密：\n");
             printf("请输入加密文件名：");
             scanf("%s",szSource);
             printf("输出的文件名为：");
             scanf("%s",szDestination);
             printf("输入你的密钥:");
             scanf("%s",szPassword);

             if(EncryptFilessh(szSource, szDestination, szPassword))
             {
             printf("加密文件%s成功 \n", szSource);
             printf("密文输出在：%s.\n",szDestination);
             }
             else
             {
                printf("加密出错\n");
             }


            break;

            case 2:


             // Call Decryptfile to do the actual decryption.   解密文件
             printf("------------------------------------------------------------\n");
             printf("解密：\n");
             printf("请输入解密文件");
             scanf("%s",szSource);
             printf("输出文件名为：: ");
             scanf("%s",szDestination);
             printf("请输入密钥：");
             scanf("%s",szPassword);

             if(DecryptFile(szSource, szDestination, szPassword))
             {
             printf("解密文件%s成功\n", szSource);
             printf("解密后的文件为%s\n",szDestination);
             }
             else
             {
                printf("解密出错！\n");
             }

            break;

            case 3:

             // Call SignFile to do the actual signature             签名文件
             printf("------------------------------------------------------------\n");
             printf("签名\n");
             printf("输入需要签名的文件");
             scanf("%s",szSource);
             printf("签名值：");
             scanf("%s",szDestination);

             if(SignFile(szSource, szDestination))
             {
                 printf("签名文件%s成功\n", szSource);

             }
             else
             {
                printf("签名文件出错\n");
             }

             // Call VerifyFile to do the actual verification   验证签名
         /*  printf("------------------------------------------------------------\n");
             printf("Verify a file and its signature.\n");
             printf("Enter the name of the file to be verified: ");
             scanf("%s",szSource);
             printf("Enter the name of the signature file: ");
             scanf("%s",szDestination);
             //printf("/nEnter the name of the public key file: ");
             //scanf("%s",szDestination);

             if(VerifyFile(szSource, szDestination))
             {
                 printf("Verification of the file %s was a success. \n", szSource);
             }
             else
             {
                 printf("Verification failed. Error file!\n");
             }*/
             break;
             case 4:
                return 0;
             default:
             printf("输入错误\n");
             break;
        }
     }

}
