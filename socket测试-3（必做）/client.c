#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#define MAX 1024

char *SERVER_HOST = "127.0.0.1";
int sock, count;
struct sockaddr_in server_addr;
int client_init(char* SERVER_PORT)
{
    printf("客户端初始化\n");
    printf("创建套接字\n");
    sock = socket(AF_INET, SOCK_STREAM,0);
    printf("填充套接字\n");
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(SERVER_HOST);
    server_addr.sin_port = htons(atoi(SERVER_PORT));

    printf("连接中.......\n");
    count = connect(sock, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if(count < 0)
    {
        printf("连接失败\n");
        exit(3);
    }
    printf("连接成功\n");
    printf("初始化完成\n");
}

int main(int argc, char *argv[])
{
    int n;
    if(argc != 2)
    {
        fprintf(stderr,"%s\n","usage: port");
        exit(0);
    }
    char line[MAX], ans[MAX];
    client_init(argv[1]);
    printf("开始会话\n");
    while(1)
    {
        bzero(line, MAX);
        fgets(line,MAX,stdin);
        line[strlen(line)-1] = 0;
        write(sock, line, MAX);
        if(0 == strcmp("q",line))
            {
                printf("结束会话\n");
                close(sock);
                return 0;
            }
        read(sock, ans, MAX);
        printf("%s\n",ans);
    }
}

