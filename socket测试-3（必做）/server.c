#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define MAX 1024
struct sockaddr_in server_addr, client_addr;
int mysock, csock;
int count, len;
void server_init(char* SERVER_PORT)
{
    printf("初始化TCP服务器\n");
    printf("创建TCP套接字\n");
    mysock = socket(AF_INET, SOCK_STREAM, 0);
    if(mysock < 0)
    {
        printf("调用套接字失败\n");
        exit(1);
    }
    printf("填充套接字\n");
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(atoi(SERVER_PORT));
    printf("绑定socket\n");
    count = bind(mysock,(struct sockaddr*)&server_addr, sizeof(server_addr));
    if(count < 0)
    {
        printf("绑定套接字失败\n");
        exit(3);
    }
    printf("开始监听\n");
    listen(mysock,5);
    printf("初始化结束\n");

}
int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        fprintf(stderr,"%s\n","usage: port");
        exit(0);
    }
    server_init(argv[1]);
	printf("等待客户端连接...\n");

	while(1)
	{
		struct sockaddr_in  ;
		len = sizeof(client_addr);
		int csock = accept(mysock,(struct sockaddr*)&client_addr,&len);
		if(csock < 0)
		{
			perror("accept");
			continue;
		}
		pid_t pid = fork();
		if(pid < 0){
                perror( "fork");
                exit(1);
		}
		else if(pid == 0)
		{
		    pid_t pid_s = getpid();
		    pid_t pid_f = getppid();
			char buf[MAX], ans[MAX];
            close(mysock);
			while(1)
            {
                char tmpBuf[MAX];
			    memset(buf, 0 ,MAX);
			    memset(ans, 0 ,MAX);
			    printf("========connect [ip:port] = [%s:%d]========\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
			    read(csock,buf,MAX);
			    if(0 == strcmp("q",buf))
				{
				    printf("客户端pid:%d 关闭, 服务器监听中\n",pid_s);
					close(csock);
					return 0;
				}
				time_t *timep = malloc(sizeof(*timep));
			    time(timep);
			    char *s = ctime(timep);
			    sprintf(ans,"客户端IP:%s\n",inet_ntoa(client_addr.sin_addr));
			    strcat(ans,"服务器实现者学号:20191304\n");
			    strcat(ans,"当前时间:");
			    strcat(ans,s);
                count = write(csock,ans,MAX);
                printf("echo:%s\nwrote:%dbytes\npid:%d\n",ans,count,pid_s);
			}
		}
		else if(pid > 0)
        {
            close(csock);
        }
	}
}
