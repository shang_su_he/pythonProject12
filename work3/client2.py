from socket import *

IP = '127.0.0.1'
SERVER_PORT = 50000
BUFLEN = 1024

# 实例化一个socket对象，指明协议
dataSocket = socket(AF_INET, SOCK_STREAM)

# 连接服务端socket
dataSocket.connect((IP, SERVER_PORT))


def encrpt(s):
    length = len(s)
    j = ""
    for i in s:
        if ord(i) <= 49:
            i = chr(ord(i) + length)
            j = j + i
        elif ord(i) > 81 and ord(i) <= 126:
            i = chr(ord(i) - length)
            j = j + i
        else:
            j = j + chr(32) + i
    return j

while True:


    s = input("请输入您想要发送的信息：")
    if  s =='exit':
        break
    enscript_s = ""
    if len(s) < 6 or len(s) > 16:
        print("长度不符合")
    if len(s) == 0:
        print("不能为空")
    for i in s:
        if ord(i) < 33 and ord(i) > 126:
            print("非法字符")

    if len(s) >= 6 and len(s) <= 16:
        enscript_s = encrpt(s)
        print(f'加密后信息为:{enscript_s}')


    # 从终端读入用户输入的字符串
    toSend = enscript_s

    # 发送消息，也要编码为 bytes
    dataSocket.send(toSend.encode())

    # 等待接收服务端的消息
    recved = dataSocket.recv(BUFLEN)
    # 如果返回空bytes，表示对方关闭了连接
    if  not recved:
        break
    # 打印读取的信息
    print(recved.decode())

dataSocket.close()